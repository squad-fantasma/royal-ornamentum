import MyApp from "./src/router/router";
import Login from "./src/pages/Login";
import Favorite from "./src/pages/Favorite";
import Cart from "./src/pages/Cart";
import CompleteCart from "./src/pages/CompleteCart";
import CompleteFavorite from "./src/pages/CompleteFavorite";
import { MySell } from "./src/pages/MySell";
import SellProduct from "./src/pages/SellProduct";
import User from "./src/pages/User";
import Home from "./src/pages/Home";
import PaymentMethod from "./src/pages/PaymentMethod";
import Cadastro from "./src/pages/Cadastro";
import PagEndereço from "./src/pages/PagEndereço";

export default function App() {
  return (
    <>
      {/* <Login /> */}
      {/* <Favorite /> */}
      {/* <Cart /> */}
      {/* <CompleteCart /> */}
      {/* <CompleteFavorite /> */}
      {/* <MySell /> */}
      <MyApp />
      {/* <SellProduct /> */}
      {/* <User /> */}
      {/* <Home /> */}
      {/* <PaymentMethod /> */}
      {/* <Cadastro /> */}
      {/* <PagEndereço /> */}
    </>
  );
}
