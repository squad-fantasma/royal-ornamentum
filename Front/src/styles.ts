import styled from "styled-components/native";

export const Titulo = styled.Text`
  color: white;
  font-size: 44px;
  margin-top: 90px;
  font-family: sans-serif;
  font-weight: bold;
  text-align: center;
`;

export const ContainerTitulo = styled.View`
  background-color: #8d3aec;
  width: 100%;
  height: 23%;
  border-bottom-left-radius: 50px;
`;

export const Touch = styled.TouchableOpacity`
  position: absolute;
  left: 10px;
  top: 38px;
  z-index: 1;
`;

export const Left = styled.Image`
  width: 50px;
  height: 50px;
`;

export const Container = styled.View`
  width: 100%;
  background-color: #fff;
`;
export const Container2 = styled.View`
  width: 85%;
`;

export const Screen = styled.View`
  flex: 1;
  display: flex;
  align-items: center;
  background-color: white;
`;

export const Text = styled.Text`
  font-size: 16px;
  font-weight: 600;
`;

export const Input = styled.TextInput`
  width: 100%;
  padding: 10px;
  background-color: #f1f2f5;
  border-radius: 8px;
  font-size: 14px;
  margin-bottom: 24px;
`;

export const TextInputEstilo = styled.TextInput`
  margin-left: 40px;
  margin-top: 40px;
  width: 80%;
  height: 47px;
  background-color: #f1f2f5;
  border-radius: 20px;
`;

export const BotaoInteragir = styled.TouchableOpacity`
  margin-left: 40px;
  margin-top: 20px;
  width: 80%;
  height: 46px;
  background-color: #8d3aec;
  border-radius: 24px;
`;

export const CadastroTexto = styled.Text`
  margin-left: 23px;
  width: 80%;
  height: 46px;
  text-align: center;
  margin-top: 5px;
  color: white;
  font-size: 20px;
`;

export const TextoLogar = styled.TouchableOpacity`
  font-size: 12px;
  color: #7827e6;
  font-weight: bold;
`;

export const ContainerItens = styled.View`
  margin-top: 50px;
`;

export const TextoFinal = styled.Text`
  margin-top: 40px;
  text-align: center;
`;

export const TextoCaixa = styled.Text`
  font-size: 10px;
  margin-left: 30px;
`;

export const ViewTextInput = styled.View`
  align-items: center;
  flex: 1;
  flex-direction: row;
`;

export const ViewTop = styled.View`
  background-color: #7827e6;
  width: 100%;
  height: 13%;
`;

export const Header1 = styled.Text`
  color: black;
  font-size: 33px;
  margin-top: 30px;
  margin-bottom: 30px;
  font-family: sans-serif;
  font-weight: bold;
  text-align: center;
`;

export const Header3 = styled.Text`
  color: #262626;
  font-size: 21px;
  margin-left: 19px;
  margin-top: 30px;
  font-family: sans-serif;
  font-weight: bold;
  padding: 25px;
`;

export const BotaoAdicionarCarrinho = styled.TouchableOpacity`
  background-color: #8d3aec;
  width: 100%;
  height: 8%;
  position: fixed;
  bottom: 0;
`;

export const AdicionarCarrinhoEstilo = styled.Text`
  color: white;
  text-align: center;
  padding: 23px;
  font-size: 18px;
  font-family: sans-serif;
  font-weight: bold;
`;
