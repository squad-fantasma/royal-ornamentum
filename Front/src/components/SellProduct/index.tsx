import React from "react";
import { TouchableOpacity } from "react-native";
import {
  Shadow,
  Grid,
  Name,
  Trash,
  Price,
  Flex,
  Flex2,
  Div,
  Image,
  Pencil,
} from "./style";

type ProductInformation = {
  name: string;
  price: number;
  img: string;
};

function SellProduct({ name, price, img }: ProductInformation) {
  return (
    <Shadow>
      <Grid>
        <Image source={require(`../../../assets/${img}.png`)}></Image>
        <Flex>
          <Name>{name}</Name>
          <TouchableOpacity>
            <Pencil
              source={require("../../../assets/ph_pencil-duotone.png")}
            ></Pencil>
          </TouchableOpacity>
        </Flex>
        <Flex2>
          <Price>${price}</Price>
          <Div>
            <TouchableOpacity>
              <Trash source={require("../../../assets/bi_trash.svg")}></Trash>
            </TouchableOpacity>
          </Div>
        </Flex2>
      </Grid>
    </Shadow>
  );
}

export default SellProduct;
