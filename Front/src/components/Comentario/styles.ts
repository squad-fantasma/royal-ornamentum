import styled from "styled-components/native";

export const RetanguloComment = styled.View`
  background-color: white;
  box-shadow: 0 2.4rem 4.8rem rgba(0, 0, 0, 0.075);
  width: 90%;
  height: 8%;
  margin-top: 40px;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

export const ViewColumn = styled.View`
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 10px;
`;

export const ViewColumn2 = styled.View`
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const ViewRow = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 40%;
`;

export const ViewIcone = styled.View`
  width: 30%;
`;

export const UsernameEstilo = styled.Text`
  color: black;
  font-family: sans-serif;
  font-size: 12px;
`;
