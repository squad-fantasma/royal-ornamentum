import React, { useState } from "react";
import { CheckBox } from "react-native-elements";
import { Image, Text, View } from "react-native";
import {
  RetanguloComment,
  UsernameEstilo,
  ViewColumn,
  ViewColumn2,
  ViewIcone,
  ViewRow,
} from "./styles";
import Icon from "react-native-vector-icons/FontAwesome5";
import Icon2 from "react-native-vector-icons/FontAwesome";
import Icon3 from "react-native-vector-icons/Foundation";

type infoComment = {
  nome: string;
  comentario: string;
  iconeperfil: string;
};

const Comentario = ({ nome, comentario, iconeperfil }: infoComment) => {
  const [isSelected, setSelected] = useState(false);
  const [isSelected2, setSelected2] = useState(false);
  return (
    <RetanguloComment>
      <ViewIcone>
        <Image
          style={{ width: "80%", height: "80%" }}
          source={require(`../../../assets/${iconeperfil}`)}
        />
      </ViewIcone>

      <ViewColumn2>
        <UsernameEstilo>{`${nome}`}</UsernameEstilo>
        <Text>{`${comentario}`}</Text>
      </ViewColumn2>

      <ViewColumn>
        <ViewRow>
          <Icon2 style={{ width: "25px", height: "25px" }} name="star" />
          <Icon2 style={{ width: "25px", height: "25px" }} name="star" />
          <Icon2 style={{ width: "25px", height: "25px" }} name="star" />
          <Icon2 style={{ width: "25px", height: "25px" }} name="star" />
          <Icon2 style={{ width: "25px", height: "25px" }} name="star-o" />
        </ViewRow>

        <ViewRow>
          <CheckBox
            uncheckedIcon="thumbs-up"
            checkedIcon="thumbs-up"
            checkedColor="green"
            uncheckedColor="black"
            style={{ width: "25px", height: "25px", marginRight: "100px" }}
            checked={isSelected}
            onPress={() => setSelected(!isSelected)}
          />
          <CheckBox
            uncheckedIcon="thumbs-down"
            checkedIcon="thumbs-down"
            checkedColor="red"
            uncheckedColor="black"
            checked={isSelected2}
            onPress={() => setSelected2(!isSelected2)}
          />
        </ViewRow>
      </ViewColumn>

      <Icon style={{ width: "20px", height: "20px" }} name="ellipsis-h">
        {" "}
      </Icon>
    </RetanguloComment>
  );
};

export default Comentario;
