import styled from "styled-components/native";

export const Shadow = styled.View`
  margin-top: 38px;
  padding: 10px;
  box-shadow: 0 2.4rem 4.8rem rgba(0, 0, 0, 0.075);
`;

export const Grid = styled.View`
  display: grid;
  grid-template-columns: 90px 1fr;
`;

export const Image = styled.Image`
  width: 78px;
  height: 90px;
  grid-row-start: 1;
  grid-row-end: 3;
`;

export const Flex = styled.View`
  display: flex;
  flex-direction: row;
  align-items: start;
  justify-content: space-between;
`;
export const Flex2 = styled.View`
  display: flex;
  flex-direction: row;
  align-items: end;
  justify-content: space-between;
`;

export const Name = styled.Text`
  font-size: 16px;
  font-weight: 500;
`;

export const Trash = styled.Image`
  width: 25px;
  height: 25px;
`;

export const Price = styled.Text`
  font-size: 16px;
  font-weight: 700;
`;

export const Div = styled.View`
  display: flex;
  flex-direction: row;
  gap: 10px;
  align-items: center;
`;

export const Less = styled.Image`
  width: 25px;
  height: 25px;
`;

export const Quantity = styled.Text`
  font-size: 16px;
  font-weight: 600;
`;

export const Plus = styled.Image`
  width: 25px;
  height: 25px;
`;
