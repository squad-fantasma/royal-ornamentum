import React from "react";
import { TouchableOpacity } from "react-native";
import {
  Div,
  Flex,
  Grid,
  Image,
  Less,
  Name,
  Plus,
  Price,
  Quantity,
  Shadow,
  Trash,
  Flex2,
} from "./style";

type ProductInformation = {
  name: string;
  price: number;
  img: string;
};

function CartProduct({ name, price, img }: ProductInformation) {
  return (
    <Shadow>
      <Grid>
        <Image source={require(`../../../assets/${img}`)}></Image>
        <Flex>
          <Name>{name}</Name>
          <TouchableOpacity>
            <Trash source={require("../../../assets/bi_trash.svg")}></Trash>
          </TouchableOpacity>
        </Flex>
        <Flex2>
          <Price>${price}</Price>
          <Div>
            <TouchableOpacity>
              <Less
                source={require("../../../assets/mdi_minus-box.png")}
              ></Less>
            </TouchableOpacity>
            <Quantity>1</Quantity>
            <TouchableOpacity>
              <Plus
                source={require("../../../assets/pajamas_file-addition-solid.png")}
              ></Plus>
            </TouchableOpacity>
          </Div>
        </Flex2>
      </Grid>
    </Shadow>
  );
}

export default CartProduct;
