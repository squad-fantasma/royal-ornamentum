import styled from "styled-components/native";

export const RetanguloEnde = styled.View`
  background-color: white;
  width: 80%;
  height: 50%;
  box-shadow: 0 2.4rem 4.8rem rgba(0, 0, 0, 0.2);
  margin-top: 40px;
  margin-left: 40px;
  margin-bottom: 40px;
`;

export const TextoNome = styled.Text`
  color: black;
  padding: 23px;
  font-size: 18px;
  font-family: Inter;
  font-weight: bold;
`;

export const TextoInfo = styled.Text`
  color: #000000;
  padding: 10px;
  font-size: 18px;
  font-family: Inter;
`;
