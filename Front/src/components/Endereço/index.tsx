import React from "react";
import {Text} from "react-native"
import { RetanguloEnde, TextoInfo, TextoNome } from "./style";

type infoEndereço = {
    nome: string;
    CEP: string;
    Rua: string;
    Número: string;
    Bairro: string;
    Complemento: string;
}

const Endereço = ({ nome,CEP,Rua,Número,Bairro,Complemento }: infoEndereço) => {
    return (
        <RetanguloEnde>
            <TextoNome>{`${nome}`}</TextoNome>
            <TextoInfo>CEP: {`${CEP}`}</TextoInfo>
            <TextoInfo>Rua: {`${Rua}`}</TextoInfo>
            <TextoInfo>Número: {`${Número}`}</TextoInfo>
            <TextoInfo>Bairro: {`${Bairro}`}</TextoInfo>
            <TextoInfo>Complemento: {`${Complemento}`}</TextoInfo>
        </RetanguloEnde>
    );
} 

export default Endereço;