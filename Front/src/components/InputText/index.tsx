import React from "react";
import { TextInputEstilo, ViewTextInput } from "../../styles";
import Icon from "react-native-vector-icons/FontAwesome";

type info = {
  textoplaceholder: string;
  iconplaceholder: string;
  iconplaceholder2: string;
};

const InputTexto = ({
  textoplaceholder,
  iconplaceholder,
  iconplaceholder2,
}: info) => {
  return (
    <ViewTextInput>
      <Icon name={`${iconplaceholder}`}></Icon>
      <TextInputEstilo placeholder={`${textoplaceholder}`}></TextInputEstilo>
      <Icon style={{ marginTop: "40px" }} name={`${iconplaceholder2}`}></Icon>
    </ViewTextInput>
  );
};

export default InputTexto;
