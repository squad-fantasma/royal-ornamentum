import { Flex, Left, Logo, LogoText, Top, Touch } from "./style";
import React from "react";

function SubHeader() {
  return (
    <>
      <Top>
        <Flex>
          <Logo source={require("../../../assets/ostra3 1.png")}></Logo>
          <LogoText
            source={require("../../../assets/Royal Ornamentum.png")}
          ></LogoText>
        </Flex>
      </Top>
    </>
  );
}

export default SubHeader;
