import styled from "styled-components/native";

export const Touch = styled.TouchableOpacity`
  position: absolute;
  left: 10px;
  top: 38px;
  z-index: 1;
`;

export const Left = styled.Image`
  width: 50px;
  height: 50px;
`;

export const Top = styled.View`
  width: 100%;
  background-color: #8d3aec;
  height: 118px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Flex = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 15px;
`;

export const Logo = styled.Image`
  width: 59px;
  height: 60px;
`;

export const LogoText = styled.Image`
  width: 120px;
  height: 54px;
`;
