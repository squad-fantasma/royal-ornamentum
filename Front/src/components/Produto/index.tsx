import React, { useState } from "react";
import { CheckBox } from "react-native-elements";
import { Image } from "react-native";
import {
  ContainerProduto,
  NomeProdutoTexto,
  ViewNomeFavorito,
  ViewEstrelaAvaliacao,
  ViewEstrelas,
  ViewInfoFinais,
  CategoriaTexto,
  DescricaoTexto,
  PrecoTexto,
  Text,
  PrecoTexto2,
} from "./style";

import Icon from "react-native-vector-icons/FontAwesome";

type infoProduto = {
  nomeProduto: string;
  imagem: string;
  categoria: string;
  preço: string;
  descricaoProduto: string;
};

const Produto = ({
  nomeProduto,
  imagem,
  categoria,
  preço,
  descricaoProduto,
}: infoProduto) => {
  const [isSelected, setSelected] = useState(false);
  return (
    <ContainerProduto>
      <Image
        style={{ width: "100%", height: "100%" }}
        source={require(`../../../assets/${imagem}`)}
      />

      <ViewNomeFavorito>
        <NomeProdutoTexto>{`${nomeProduto}`}</NomeProdutoTexto>

        <CheckBox
          uncheckedIcon="heart-o"
          checkedIcon="heart"
          checkedColor="red"
          uncheckedColor="black"
          checked={isSelected}
          onPress={() => setSelected(!isSelected)}
        />
      </ViewNomeFavorito>

      <ViewEstrelaAvaliacao>
        <CategoriaTexto>{`${categoria}`}</CategoriaTexto>
        <ViewEstrelas>
          <Icon style={{ marginTop: "5px" }} name="star" />
          <Icon style={{ marginTop: "5px" }} name="star" />
          <Icon style={{ marginTop: "5px" }} name="star" />
          <Icon style={{ marginTop: "5px" }} name="star" />
          <Icon style={{ marginTop: "5px" }} name="star" />
          <Text>4.0</Text>
        </ViewEstrelas>
      </ViewEstrelaAvaliacao>

      <ViewInfoFinais>
        <PrecoTexto>{`${preço}`}</PrecoTexto>
        <PrecoTexto2>Descrição</PrecoTexto2>
        <DescricaoTexto>{`${descricaoProduto}`}</DescricaoTexto>
      </ViewInfoFinais>
    </ContainerProduto>
  );
};

export default Produto;
