import styled from "styled-components/native";

export const ViewProduto = styled.View`
  padding: 20px;
`;

export const ContainerProduto = styled.View`
  width: 100%;
  height: 40%;
`;

export const NomeProdutoTexto = styled.Text`
  color: black;
  padding: 23px;
  font-size: 24px;
  font-family: Inter;
  font-weight: bold;
`;

export const CategoriaTexto = styled.Text`
  color: #9d9ea0;
  margin-left: 23px;
  font-size: 18px;
  font-family: Inter;
  font-weight: 400;
`;

export const PrecoTexto = styled.Text`
  color: black;
  margin-left: 23px;
  margin-top: 8px;
  font-size: 18px;
  margin-bottom: 35px;
  font-family: Inter;
  font-weight: bold;
`;
export const PrecoTexto2 = styled.Text`
  color: black;
  margin-left: 23px;
  margin-top: 8px;
  font-size: 18px;
  margin-bottom: 10px;
  font-family: Inter;
  font-weight: bold;
`;

export const DescricaoTexto = styled.Text`
  color: #9d9ea0;
  margin-left: 23px;
  font-size: 16px;
  font-family: Inter;
  font-weight: 400;
`;

export const ViewNomeFavorito = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ViewEstrelaAvaliacao = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 0px;
`;

export const Text = styled.Text`
  font-size: 14px;
  margin-left: 3px;
`;

export const ViewEstrelas = styled.View`
  flex: 1;
  flex-direction: row;
  margin-left: 100px;
`;

export const ViewInfoFinais = styled.View`
  margin-top: 20px;
  flex: 1;
  flex-direction: column;
  align-items: space-around;
`;
