import React, {useState} from "react";
import {CheckBox} from 'react-native-elements'


type info = {
    texto: string;
}
const Checkbox = ({texto}:info) => {
    const [isSelected, setSelected] = useState(false);
    return (
        <CheckBox
            title={`${texto}`}
            uncheckedIcon="square-o"
            checkedIcon="check-square-o"
            checkedColor="green"
            uncheckedColor="red"
            checked={isSelected}
            onPress={() => setSelected(!isSelected)}
        />

    );
} 

export default Checkbox;