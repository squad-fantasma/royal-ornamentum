import { useNavigation } from "@react-navigation/native";
import React from "react";
import { BotaoAdicionarCarrinho, AdicionarCarrinhoEstilo } from "../../styles";

const AdicionarAoCarrinho = () => {
  const navigation = useNavigation();
  return (
    <BotaoAdicionarCarrinho onPress={() => navigation.navigate("CompleteCart")}>
      <AdicionarCarrinhoEstilo>Adicionar ao carrinho</AdicionarCarrinhoEstilo>
    </BotaoAdicionarCarrinho>
  );
};

export default AdicionarAoCarrinho;
