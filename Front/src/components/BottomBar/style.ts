import styled from "styled-components/native";

export const BottomContainer = styled.View`
  width: 100%;
  height: 80px;
  background-color: #e9e9e9;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  gap: 56px;
  position: fixed;
  bottom: 0px;
`;

export const Div = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Icon = styled.Image`
  width: 36px;
  height: 36px;
`;

export const IconText = styled.Text`
  font-size: 14px;
`;
