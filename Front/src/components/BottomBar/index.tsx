import React from "react";
import { BottomContainer, Div, Icon, IconText } from "./style";
import { useNavigation } from "@react-navigation/native";
import Home from "../../pages/Home";

function BottomBar() {
  const navigation = useNavigation();
  return (
    <BottomContainer>
      <Div onPress={() => navigation.navigate("Home")}>
        <Icon source={require("../../../assets/Vector (1).svg")}></Icon>
        <IconText>Home</IconText>
      </Div>
      <Div onPress={() => navigation.navigate("CompleteFavorite")}>
        <Icon
          source={require("../../../assets/mdi_cards-heart (2).png")}
        ></Icon>
        <IconText>Favorito</IconText>
      </Div>
      <Div onPress={() => navigation.navigate("SellProduct")}>
        <Icon
          source={require("../../../assets/Vector (2).svg")}
          style={{ width: 18, height: 36 }}
        ></Icon>
        <IconText>Vender</IconText>
      </Div>
      <Div onPress={() => navigation.navigate("User")}>
        <Icon source={require("../../../assets/Vector (3).svg")}></Icon>
        <IconText>Perfil</IconText>
      </Div>
    </BottomContainer>
  );
}

export default BottomBar;
