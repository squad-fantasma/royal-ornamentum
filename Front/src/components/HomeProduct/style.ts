import styled from "styled-components/native";

export const Div = styled.TouchableOpacity`
  display: flex;
  align-items: center;
`;
export const Position = styled.TouchableOpacity`
  position: absolute;
  right: 0;
  z-index: 1;
`;

export const Heart = styled.Image`
  width: 30px;
  height: 30px;
  box-shadow: 0 2.4rem 4.8rem rgba(0, 0, 0, 0.075);
`;

export const Photo = styled.Image`
  width: 130px;
  height: 150px;
  position: relative;
`;

export const Name = styled.Text`
  font-size: 14px;
`;
export const Price = styled.Text`
  font-size: 14px;
`;
