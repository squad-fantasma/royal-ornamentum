import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { Div, Position, Heart, Photo, Name, Price } from "./style";

type ProductInformation = {
  name: string;
  price: number;
  img: string;
};

function HomeProduct({ name, price, img }: ProductInformation) {
  const [isFavorite, setIsFavorite] = useState(false);
  let heartColor: string;

  if (isFavorite) {
    heartColor = "#DC1010 ";
  } else {
    heartColor = "#9D9EA0";
  }
  const navigation = useNavigation();
  return (
    <Div onPress={() => navigation.navigate("PagProduto")}>
      <Position onPress={() => setIsFavorite(!isFavorite)}>
        <Heart
          style={{ tintColor: heartColor }}
          source={require("../../../assets/mdi_cards-heart (2).png")}
        ></Heart>
      </Position>
      <Photo source={require(`../../../assets/${img}`)}></Photo>
      <Name>{name}</Name>
      <Price>${price}</Price>
    </Div>
  );
}

export default HomeProduct;
