import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { TouchableOpacity } from "react-native";
import {
  Shadow,
  Grid,
  Flex,
  Name,
  Heart,
  Price,
  Div,
  Image,
  ButtonText,
  Button,
} from "./style";

type ProductInformation = {
  name: string;
  price: number;
  img: string;
};

function FavoriteProduct({ name, price, img }: ProductInformation) {
  const [isFavorite, setIsFavorite] = useState(false);
  let heartColor: string;

  if (isFavorite) {
    heartColor = "#9D9EA0";
  } else {
    heartColor = "#DC1010";
  }
  const navigation = useNavigation();
  return (
    <Shadow>
      <Grid>
        <Image source={require(`../../../assets/${img}`)}></Image>
        <Flex>
          <Name>{name}</Name>
          <TouchableOpacity onPress={() => setIsFavorite(!isFavorite)}>
            <Heart
              style={{ tintColor: heartColor }}
              source={require("../../../assets/mdi_cards-heart (1).png")}
            ></Heart>
          </TouchableOpacity>
        </Flex>
        <Flex>
          <Price>${price}</Price>
        </Flex>
      </Grid>
      <Div>
        <Button onPress={() => navigation.navigate("CompleteCart")}>
          <ButtonText>Adicionar ao carrinho</ButtonText>
        </Button>
      </Div>
    </Shadow>
  );
}

export default FavoriteProduct;
