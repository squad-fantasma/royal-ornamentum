import styled from "styled-components/native";

export const Shadow = styled.View`
  margin-top: 38px;
  padding: 10px;
  box-shadow: 0 2.4rem 4.8rem rgba(0, 0, 0, 0.075);
`;

export const Grid = styled.View`
  display: grid;
  grid-template-columns: 90px 1fr;
`;

export const Image = styled.Image`
  width: 78px;
  height: 90px;
  grid-row-start: 1;
  grid-row-end: 3;
`;

export const Flex = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Name = styled.Text`
  font-size: 16px;
  font-weight: 500;
`;

export const Heart = styled.Image`
  width: 25px;
  height: 25px;
`;

export const Price = styled.Text`
  font-size: 16px;
  font-weight: 700;
`;

export const Div = styled.View`
  display: flex;
  align-items: center;
`;

export const Button = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  width: 100%;
  max-width: 400px;
  height: 40px;
  margin-top: 15px;
  border-radius: 16px;
  background-color: #8d3aec;
`;

export const ButtonText = styled.Text`
  font-size: 16px;
  line-height: 21;
  font-weight: 500;
  letter-spacing: 0.25;
  color: white;
  text-align: center;
`;
