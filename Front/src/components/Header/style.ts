import styled from "styled-components/native";

export const Top = styled.View`
  width: 100%;
  background-color: #8d3aec;
  height: 118px;
  display: flex;
  justify-content: center;
  padding-left: 20px;
  padding-right: 20px;
`;

export const Flex = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 15px;
`;

export const Logo = styled.Image`
  width: 58px;
  height: 58px;
`;

export const Search = styled.TextInput`
  width: 80%;
  padding: 16px 15px;
  padding-right: 50px;
  font-weight: 600;
  height: 32px;
  background-color: #f1f2f5;
  border-radius: 24px;
  font-size: 12px;
`;

export const Cart = styled.Image`
  margin-left: 10px;
  width: 28px;
  height: 28px;
  z-index: 1;
`;
