import React from "react";
import { Cart, Flex, Logo, Search, Top } from "./style";
import { TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

function Header() {
  const navigation = useNavigation();
  return (
    <Top>
      <Flex>
        <Logo source={require("../../../assets/ostra3 1.png")}></Logo>
        <Search
          placeholder="Buscar produto"
          placeholderTextColor={"#9d9ea0"}
        ></Search>
        <TouchableOpacity onPress={() => navigation.navigate("CompleteCart")}>
          <Cart source={require("../../../assets/Vector.svg")}></Cart>
        </TouchableOpacity>
      </Flex>
    </Top>
  );
}

export default Header;
