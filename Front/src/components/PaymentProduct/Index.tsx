import React from "react";
import { TouchableOpacity } from "react-native";
import { Shadow, Flex, TextProduct, Plus } from "./style";

function PaymentProduct() {
  return (
    <Shadow>
      <Flex>
        <TextProduct>Adicionar cartão</TextProduct>
        <TouchableOpacity>
          <Plus
            source={require("../../../assets/pajamas_file-addition-solid.png")}
          ></Plus>
        </TouchableOpacity>
      </Flex>
    </Shadow>
  );
}

export default PaymentProduct;
