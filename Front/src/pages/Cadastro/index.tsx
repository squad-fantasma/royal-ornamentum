import React from "react";
import { Alert, TextInput } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  TextoFinal,
  ContainerItens,
  TextoLogar,
  BotaoInteragir,
  CadastroTexto,
  TextInputEstilo,
  Container,
  ContainerTitulo,
  Titulo,
  Text,
  Input,
  Container2,
  Screen,
} from "../../styles";
import Caixa from "../../components/Checkbox";
import Textoinput from "../../components/InputText";
import { useForm, Controller } from "react-hook-form";

export default function Cadastro() {
  const navigation = useNavigation();
  return (
    <Screen>
      <ContainerTitulo>
        <Titulo> Crie sua conta </Titulo>
      </ContainerTitulo>
      <Container2>
        <ContainerItens>
          <Input
            placeholder="nome completo"
            placeholderTextColor={"#9D9EA0"}
          ></Input>

          <Input placeholder="Email" placeholderTextColor={"#9D9EA0"}></Input>

          <Input
            placeholder="Numero de telefone"
            placeholderTextColor={"#9D9EA0"}
          ></Input>

          <Input
            placeholder="Data de nascimento"
            placeholderTextColor={"#9D9EA0"}
          ></Input>

          <Input placeholder="Senha" placeholderTextColor={"#9D9EA0"}></Input>

          <Input
            placeholder="Confirmar Senha"
            placeholderTextColor={"#9D9EA0"}
          ></Input>
        </ContainerItens>
        {/* <Caixa texto="Quero receber ofertas e novidades por email."></Caixa> */}
        <Caixa texto="Li e concordo com as políticas da empresa e políticas de privacidade."></Caixa>
        <BotaoInteragir onPress={() => navigation.navigate("Login")}>
          <CadastroTexto>Cadastrar</CadastroTexto>
        </BotaoInteragir>
        <TextoFinal>
          Já tem conta?
          <TextoLogar onPress={() => navigation.navigate("Login")}>
            <Text>Logue agora</Text>
          </TextoLogar>
        </TextoFinal>
      </Container2>
    </Screen>
  );
}
