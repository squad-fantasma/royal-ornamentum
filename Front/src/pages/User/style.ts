import styled from "styled-components/native";

export const Title = styled.Text`
  font-size: 32px;
  font-weight: 600;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 0px;
`;

export const Screen = styled.View`
  flex: 1;
  display: flex;
  align-items: center;
  background-color: white;
`;

export const Container = styled.View`
  width: 85%;
  margin-bottom: 20px;
  margin-bottom: 50px;
`;

export const Top = styled.View`
  width: 100%;
  background-color: #8d3aec;
  height: 118px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Flex = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 15px;
`;

export const Logo = styled.Image`
  width: 59px;
  height: 60px;
`;

export const LogoText = styled.Image`
  width: 120px;
  height: 54px;
`;

export const PhotoDiv = styled.View`
  display: flex;
  align-items: center;
`;

export const Photo = styled.Image`
  width: 200px;
  height: 200px;
  margin-bottom: 24px;
`;

export const TextDiv = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom-width: 1px;
  padding-bottom: 6px;
  margin-bottom: 16px;
`;

export const Text = styled.Text`
  font-size: 16px;
`;

export const Left = styled.Image`
  width: 30px;
  height: 30px;
`;

export const End = styled.Image`
  width: 18px;
  height: 18px;
  margin-right: 6px;
`;
