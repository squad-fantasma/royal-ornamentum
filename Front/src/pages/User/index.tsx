import React from "react";
import { TouchableOpacity } from "react-native";
import BottomBar from "../../components/BottomBar";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  End,
  Flex,
  Left,
  Logo,
  LogoText,
  Photo,
  PhotoDiv,
  Screen,
  Text,
  TextDiv,
  Title,
  Top,
} from "./style";

export default function User() {
  const navigation = useNavigation();
  return (
    <Screen>
      <Top>
        <Flex>
          <Logo source={require("../../../assets/ostra3 1.png")}></Logo>
          <LogoText
            source={require("../../../assets/Royal Ornamentum.png")}
          ></LogoText>
        </Flex>
      </Top>
      <Container>
        <Title>Perfil</Title>
        <PhotoDiv>
          <Photo
            source={require("../../../assets/mdi_user-circle.png")}
          ></Photo>
        </PhotoDiv>
        <TextDiv onPress={() => navigation.navigate("InfoPesso")}>
          <Text>Informação pessoais</Text>
          <Left
            source={require("../../../assets/material-symbols_chevron-left-rounded.png")}
          ></Left>
        </TextDiv>
        <TextDiv onPress={() => navigation.navigate("PaymentMethod")}>
          <Text>Método de pagamento</Text>
          <Left
            source={require("../../../assets/material-symbols_chevron-left-rounded.png")}
          ></Left>
        </TextDiv>
        <TextDiv onPress={() => navigation.navigate("MySell")}>
          <Text>Minhas vendas</Text>
          <Left
            source={require("../../../assets/material-symbols_chevron-left-rounded.png")}
          ></Left>
        </TextDiv>
        <TextDiv onPress={() => navigation.navigate("MudarSenha")}>
          <Text>Mudar senha</Text>
          <Left
            source={require("../../../assets/material-symbols_chevron-left-rounded.png")}
          ></Left>
        </TextDiv>
        <TextDiv onPress={() => navigation.navigate("PagEndereço")}>
          <Text>Endereços</Text>
          <Left
            source={require("../../../assets/material-symbols_chevron-left-rounded.png")}
          ></Left>
        </TextDiv>
        <TextDiv>
          <Text>Logout</Text>
          <End source={require("../../../assets/Vector (1).png")}></End>
        </TextDiv>
      </Container>
      <BottomBar />
    </Screen>
  );
}
