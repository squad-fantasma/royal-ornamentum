import styled from "styled-components/native";

export const Screen = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  background-color: white;
`;

export const Container = styled.View`
  width: 85%;
`;

export const Title = styled.Text`
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  margin-top: 40px;
`;

export const Touch = styled.TouchableOpacity`
  position: absolute;
  left: 10px;
  top: 38px;
  z-index: 1;
`;

export const Left = styled.Image`
  width: 50px;
  height: 50px;
`;

export const Flex = styled.View`
  margin-top: 70px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const FinalPrice = styled.Text`
  font-size: 20px;
  font-weight: 600;
`;

export const Text = styled.Text`
  font-size: 20px;
  font-weight: 600;
`;

export const Div = styled.View`
  display: flex;
  align-items: center;
`;

export const Button = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  width: 100%;
  max-width: 400px;
  height: 50px;
  margin-top: 30px;
  border-radius: 16px;
  background-color: #8d3aec;
`;

export const ButtonText = styled.Text`
  font-size: 20px;
  line-height: 21;
  font-weight: 500;
  letter-spacing: 0.25;
  color: white;
  text-align: center;
`;
