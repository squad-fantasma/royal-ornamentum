import React from "react";
import CartProduct from "../../components/CartProduct";
import SubHeader from "../../components/Subheader";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  Screen,
  Title,
  Flex,
  FinalPrice,
  Text,
  Button,
  ButtonText,
  Div,
  Touch,
  Left,
} from "./style";

export default function CompleteCart() {
  const navigation = useNavigation();
  return (
    <Screen>
      <Touch onPress={() => navigation.navigate("Home")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>
      <SubHeader />
      <Container>
        <Title>Meu carrinho</Title>
        <CartProduct
          name={"Anel de ouro"}
          price={500}
          img={"sabrianna-NhrcL_C0sFA-unsplash 1.png"}
        />
        <CartProduct name={"Colar"} price={500} img={"Colar2.jpg"} />
        <Flex>
          <Text> Total: </Text>
          <FinalPrice>$1000</FinalPrice>
        </Flex>
        <Div>
          <Button>
            <ButtonText>Comprar</ButtonText>
          </Button>
        </Div>
      </Container>
    </Screen>
  );
}
