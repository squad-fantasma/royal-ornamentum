import React from "react";
import {
  Container,
  Header1,
  BotaoInteragir,
  CadastroTexto,
  Touch,
  Left,
} from "../../styles";
import TopTab from "../../components/Header";
import Textoinput from "../../components/InputText";
import SubHeader from "../../components/Subheader";
import { useNavigation } from "@react-navigation/native";

export default function MudarEnde() {
  const navigation = useNavigation();
  return (
    <Container>
      <Touch onPress={() => navigation.navigate("User")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>
      <SubHeader />
      <Header1>Endereço novo</Header1>

      <Textoinput
        textoplaceholder="Nome"
        iconplaceholder=""
        iconplaceholder2=""
      ></Textoinput>
      <Textoinput
        textoplaceholder="CEP"
        iconplaceholder=""
        iconplaceholder2=""
      ></Textoinput>
      <Textoinput
        textoplaceholder="Logradouro"
        iconplaceholder=""
        iconplaceholder2=""
      ></Textoinput>
      <Textoinput
        textoplaceholder="Número"
        iconplaceholder=""
        iconplaceholder2=""
      ></Textoinput>
      <Textoinput
        textoplaceholder="Cidade"
        iconplaceholder=""
        iconplaceholder2=""
      ></Textoinput>
      <Textoinput
        textoplaceholder="Estado"
        iconplaceholder=""
        iconplaceholder2=""
      ></Textoinput>
      <Textoinput
        textoplaceholder="Bairro"
        iconplaceholder=""
        iconplaceholder2=""
      ></Textoinput>
      <Textoinput
        textoplaceholder="Complemento"
        iconplaceholder=""
        iconplaceholder2=""
      ></Textoinput>

      <BotaoInteragir>
        <CadastroTexto>Salvar</CadastroTexto>
      </BotaoInteragir>
    </Container>
  );
}
