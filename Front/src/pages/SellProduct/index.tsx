import React from "react";
import { TouchableOpacity } from "react-native";
import BottomBar from "../../components/BottomBar";
import {
  Button,
  ButtonDiv,
  ButtonText,
  Categories,
  Container,
  Div,
  Flex,
  Input,
  InputDescription,
  Logo,
  LogoText,
  Pencil,
  Photo,
  Position,
  Screen,
  Text,
  Title,
  Top,
} from "./style";
import { useNavigation } from "@react-navigation/native";

export default function SellProduct() {
  const navigation = useNavigation();
  {
    return (
      <Screen>
        <Top>
          <Flex>
            <Logo source={require("../../../assets/ostra3 1.png")}></Logo>
            <LogoText
              source={require("../../../assets/Royal Ornamentum.png")}
            ></LogoText>
          </Flex>
        </Top>
        <Container>
          <Title>Anunciar produto</Title>
          <Photo
            source={require("../../../assets/sabrianna-NhrcL_C0sFA-unsplash 1.png")}
          ></Photo>
          <Position>
            <Pencil
              source={require("../../../assets/ph_pencil-duotone.png")}
            ></Pencil>
          </Position>
          <Text>Nome do produto</Text>
          <Input></Input>
          <Text>Preço do produto</Text>
          <Input></Input>
          <Text>Selecione categoria</Text>
          <Div>
            <TouchableOpacity>
              <Categories>Anel</Categories>
            </TouchableOpacity>
            <TouchableOpacity>
              <Categories>Colar</Categories>
            </TouchableOpacity>
            <TouchableOpacity>
              <Categories>Brinco</Categories>
            </TouchableOpacity>
          </Div>
          <Text>Descrição do produto</Text>
          <InputDescription></InputDescription>
          <ButtonDiv>
            <Button onPress={() => navigation.navigate("Home")}>
              <ButtonText>Anunciar</ButtonText>
            </Button>
          </ButtonDiv>
        </Container>
        <BottomBar />
      </Screen>
    );
  }
}
