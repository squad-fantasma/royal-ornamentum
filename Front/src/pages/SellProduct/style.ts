import styled from "styled-components/native";

export const Title = styled.Text`
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 24px;
`;

export const Screen = styled.View`
  flex: 1;
  display: flex;
  align-items: center;
  background-color: white;
`;

export const Container = styled.View`
  width: 85%;
  margin-bottom: 20px;
`;

export const Top = styled.View`
  width: 100%;
  background-color: #8d3aec;
  height: 118px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Flex = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 15px;
`;

export const Logo = styled.Image`
  width: 59px;
  height: 60px;
`;

export const LogoText = styled.Image`
  width: 120px;
  height: 54px;
`;

export const Position = styled.TouchableOpacity`
  position: absolute;
  left: 84px;
  top: 58px;
  z-index: 1;
`;

export const Photo = styled.Image`
  width: 100px;
  height: 100px;
  margin-bottom: 24px;
  position: relative;
`;

export const Pencil = styled.Image`
  width: 30px;
  height: 30px;
`;

export const Text = styled.Text`
  font-size: 16px;
  font-weight: 600;
`;

export const Input = styled.TextInput`
  width: 100%;
  padding: 10px;
  background-color: #f1f2f5;
  border-radius: 8px;
  font-size: 14px;
  margin-bottom: 24px;
`;

export const Div = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 30px;
  margin-top: 8px;
  margin-bottom: 24px;
`;

export const Categories = styled.Text`
  font-size: 12px;
  border: 1px solid #a8a8a8;
  border-radius: 8px;
  padding: 10px;
  padding-left: 20px;
  padding-right: 20px;
`;

export const InputDescription = styled.TextInput`
  width: 100%;
  height: 80px;
  padding: 12px;
  background-color: #f1f2f5;
  border-radius: 8px;
  font-size: 16px;
  margin-bottom: 16px;
`;

export const ButtonDiv = styled.View`
  display: flex;
  align-items: center;
`;

export const Button = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  width: 100%;
  max-width: 400px;
  height: 40px;
  margin-top: 15px;
  border-radius: 16px;
  background-color: #8d3aec;
`;

export const ButtonText = styled.Text`
  font-size: 16px;
  line-height: 21;
  font-weight: 500;
  letter-spacing: 0.25;
  color: white;
  text-align: center;
`;
