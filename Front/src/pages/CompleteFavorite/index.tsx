import React from "react";
import BottomBar from "../../components/BottomBar";
import Header from "../../components/Header";
import FavoriteProduct from "../../components/FavoriteProduct";
import { Container, Screen, Title } from "./style";

//

export default function CompleteFavorite() {
  return (
    <Screen>
      <Header />
      <Container>
        <Title>Favoritos</Title>
        <FavoriteProduct
          name={"Anel de ouro"}
          price={500}
          img={"sabrianna-NhrcL_C0sFA-unsplash 1.png"}
        />
        <FavoriteProduct name={"Cordão"} price={500} img={"Colar2.jpg"} />
      </Container>
      <BottomBar />
    </Screen>
  );
}
