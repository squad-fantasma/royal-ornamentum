import styled from "styled-components/native";

export const Title = styled.Text`
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  margin-top: 40px;
`;

export const Screen = styled.View`
  flex: 1;
  display: flex;
  background-color: white;
  align-items: center;
`;

export const Container = styled.View`
  width: 85%;
  margin-bottom: 174px;
`;
