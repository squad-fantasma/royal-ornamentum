import React from "react";
import {
  Container,
  Header1,
  Header3,
  Touch,
  Left,
  Text,
  Input,
  Container2,
  Screen,
} from "../../styles";
import TopTab from "../../components/Header";
import Textoinput from "../../components/InputText";
import { useNavigation } from "@react-navigation/native";
import SubHeader from "../../components/Subheader";

export default function InfoPesso() {
  const navigation = useNavigation();
  return (
    <Screen>
      <Touch onPress={() => navigation.navigate("User")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>
      <SubHeader />
      <Container2>
        <Header1>Informações pessoais</Header1>
        <Text>Nome completo</Text>
        <Input></Input>
        <Text>Email</Text>
        <Input></Input>
        <Text>Data de nascimento</Text>
        <Input></Input>
        <Text>Celular</Text>
        <Input></Input>
      </Container2>
    </Screen>
  );
}
