import React from "react";

import AdicionarAoCarrinho from "../../components/AdicionarCarrinho";
import Produto from "../../components/Produto";
import Comentario from "../../components/Comentario";
import { View, Text, Image } from "react-native";
import {
  Blank,
  ComentarioEstilo,
  ComentHeader,
  ContainerPagProduto,
  Flex3,
} from "./style";
import Icon from "react-native-vector-icons/AntDesign";
import { Left, Touch, Header3 } from "../../styles";
import { useNavigation } from "@react-navigation/native";

export default function PagProduto() {
  const navigation = useNavigation();
  return (
    <ContainerPagProduto>
      <Touch onPress={() => navigation.navigate("Home")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>

      <Produto
        nomeProduto="Anel de ouro"
        imagem="Anel.jpg"
        categoria="Categoria: anel"
        preço="$500"
        descricaoProduto="Apresentamos 
            este Anel Ouro 18k Solitário Zircônia 1.05 gramas. Um lindo anel é sempre uma peça atraente, seja pelo design, brilho
            ou sofisticação. Evidencia seus gestos garantindo-lhe elegância."
      ></Produto>

      <ComentarioEstilo>Comentários</ComentarioEstilo>
      <Comentario
        nome="user22109292"
        comentario="adorei!!!"
        iconeperfil="Icone.png"
      ></Comentario>
      <Comentario
        nome="user22109292"
        comentario="adorei!!!"
        iconeperfil="Icone.png"
      ></Comentario>

      <Flex3>
        <ComentHeader>adicionar comentario</ComentHeader>
        <Image
          style={{ width: 30, height: 30 }}
          source={require("../../../assets/pajamas_file-addition-solid.png")}
        />
      </Flex3>

      <AdicionarAoCarrinho></AdicionarAoCarrinho>
      <Blank></Blank>
    </ContainerPagProduto>
  );
}
