import styled from "styled-components/native";

export const ContainerPagProduto = styled.View`
  flex: 1;
  align-items: center;
  background-color: white;
`;

export const ComentarioEstilo = styled.Text`
  font-size: 20px;
  margin-top: 350px;
`;

export const Flex3 = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 40px;
`;

export const ComentHeader = styled.Text`
  font-size: 20px;
  margin-bottom: 20px;
  margin-top: 30px;
  font-weight: 600;
`;

export const Blank = styled.View`
  width: 100px;
  height: 60px;
`;
