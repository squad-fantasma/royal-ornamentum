import React from "react";
import { useNavigation } from "@react-navigation/native";
import { useForm, Controller } from "react-hook-form";
import {
  Container,
  Title,
  PurpleBox,
  InputEmail,
  InputPassword,
  Keep,
  Forgot,
  Button,
  SingIn,
  ButtonText,
  Flex,
  SingText,
  Screen,
  Div,
  Photo,
  TextError,
  InputForm,
} from "./style";
import { TouchableOpacity, Text, StyleSheet, TextInput } from "react-native";

type DataProps = {
  email: string;
  senha: string;
};

export default function Login() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<DataProps>();

  const onSubmit = (data: DataProps) => {
    if (data.email == "ejcm@gmail.com" && data.senha == "12345678") {
      navigation.navigate("Home");
    } else {
      console.log("dados errados");
    }
  };

  const navigation = useNavigation();
  return (
    <Screen>
      <PurpleBox />
      <Title>Login</Title>

      <Container>
        <Controller
          control={control}
          name="email"
          defaultValue=""
          render={({ field: { onBlur, onChange, value } }) => (
            <InputForm
              onBlur={onBlur}
              onChangeText={(value: any) => onChange(value)}
              value={value}
              placeholder="Email"
              maxLength={256}
            />
          )}
          rules={{
            required: "Email é obrigatório",
            pattern: {
              value: /^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
              message: "Formato de email inválido",
            },
          }}
        />
        {errors.email && <TextError>{errors.email.message}</TextError>}

        <Controller
          control={control}
          name="senha"
          defaultValue=""
          render={({ field: { onBlur, onChange, value } }) => (
            <InputForm
              secureTextEntry={true}
              onBlur={onBlur}
              onChangeText={(value: any) => onChange(value)}
              value={value}
              placeholder="Senha"
              maxLength={18}
            />
          )}
          rules={{
            required: "Senha é obrigatória",
            minLength: {
              value: 6,
              message: "Senha muito curta",
            },
          }}
        />
        {errors.senha && <TextError>{errors.senha.message}</TextError>}
        <Flex>
          <Keep>Manter conectado</Keep>
          <Forgot onPress={() => navigation.navigate("EsqueciMinhaSenha")}>
            Esqueci minha senha
          </Forgot>
        </Flex>
        <Div>
          <Button onPress={handleSubmit(onSubmit)}>
            <ButtonText>Login</ButtonText>
          </Button>
        </Div>
      </Container>
      <SingIn>
        não possui conta?
        <SingText onPress={() => navigation.navigate("Cadastro")}>
          {" "}
          Registre-se aqui!
        </SingText>
      </SingIn>
    </Screen>
  );
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: "#F1F2F5",
    borderColor: "none",
    borderBottomEndRadius: 16,
    borderBottomStartRadius: 16,
    borderTopEndRadius: 16,
    borderTopStartRadius: 16,
    fontSize: 16,
    height: 50,
    padding: 10,
    borderRadius: 4,
  },
});
