import styled from "styled-components/native";

export const Screen = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  background-color: white;
`;

export const Container = styled.View`
  width: 90%;
  gap: 20px;
  margin-top: 164px;
`;

export const PurpleBox = styled.View`
  position: absolute;
  width: 100%;
  height: 197px;
  background-color: #8d3aec;
  border-bottom-left-radius: 50px;
`;

export const Title = styled.Text`
  color: #fff;
  font-size: 44px;
  margin-top: 97px;
  margin-right: 90px;
  z-index: 1;
  font-weight: 600;
`;

export const Photo = styled.Image`
  position: absolute;
  left: -175px;
  top: -100px;
  width: 50px;
  height: 50px;
`;

export const Flex = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const InputEmail = styled.TextInput`
  width: 100%;
  padding: 12px;
  background-color: white;
  border-radius: 24px;
  font-size: 18px;
`;

export const InputPassword = styled.TextInput`
  width: 100%;
  padding: 12px;
  background-color: white;
  border-radius: 24px;
  font-size: 20px;
`;

export const Keep = styled.TouchableOpacity`
  font-size: 12px;
  color: #9d9ea0;
  font-family: sans-serif;
`;

export const Forgot = styled.TouchableOpacity`
  font-size: 12px;
  color: #9d9ea0;
  font-weight: 600;
  font-family: sans-serif;
`;

export const Div = styled.View`
  display: flex;
  align-items: center;
`;

export const Button = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  width: 100%;
  max-width: 400px;
  height: 50px;
  margin-top: 20px;
  border-radius: 24px;
  background-color: #8d3aec;
`;

export const ButtonText = styled.Text`
  font-size: 20px;
  line-height: 21;
  font-weight: 600;
  letter-spacing: 0.25;
  color: white;
  text-align: center;
`;

export const SingIn = styled.Text`
  text-align: center;
  margin-top: 230px;
  margin-bottom: 20px;
  color: #9d9ea0;
`;

export const SingText = styled.TouchableOpacity`
  font-size: 14px;
  color: #8d3aec;
  font-weight: 600;
`;

export const InputForm = styled.TextInput.attrs({
  placeholderTextColor: "#9d9ea0",
})`
  background-color: #f1f2f5;
  width: 100%;
  padding: 12px;
  border-radius: 24px;
  font-size: 20px;
`;

export const TextError = styled.Text`
  color: #d65252;
  font-size: 14;
  font-weight: 400;
`;
