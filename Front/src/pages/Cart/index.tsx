import React from "react";
import SubHeader from "../../components/Subheader";
import {
  Button,
  ButtonText,
  Container,
  Content,
  Div,
  Screen,
  Shadow,
  SubContent,
  Title,
} from "./style";

//comentario

export default function Cart() {
  return (
    <Screen>
      <SubHeader />
      <Container>
        <Title>Meu carrinho</Title>
        <Shadow>
          <Content>O seu carrinho está vazio</Content>
          <SubContent>
            não sabe o que comprar? venha conferir os diversos produtos da loja
          </SubContent>
          <Div>
            <Button>
              <ButtonText>Ver ofertas</ButtonText>
            </Button>
          </Div>
        </Shadow>
      </Container>
    </Screen>
  );
}
