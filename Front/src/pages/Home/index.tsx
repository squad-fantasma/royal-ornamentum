import React from "react";
import { TouchableOpacity } from "react-native";
import BottomBar from "../../components/BottomBar";
import Header from "../../components/Header";
import HomeProduct from "../../components/HomeProduct";
import {
  Blank,
  Categories,
  CategoriesFlex,
  Container,
  Flex,
  Screen,
  Text,
  Title,
} from "./style";
import View from "react-native";

export default function Home() {
  return (
    <Screen>
      <Header />
      <Container>
        <Title>Promoções do dia</Title>
        <Flex>
          <HomeProduct name={"Anel de ouro"} price={500} img={"Anel.jpg"} />
          <HomeProduct name={"Colar Ouro"} price={350} img={"Colar.jpg"} />
        </Flex>
        <Text>Categorias</Text>
        <CategoriesFlex>
          <TouchableOpacity>
            <Categories>Todos</Categories>
          </TouchableOpacity>
          <TouchableOpacity>
            <Categories>Anel</Categories>
          </TouchableOpacity>
          <TouchableOpacity>
            <Categories>Colar</Categories>
          </TouchableOpacity>
          <TouchableOpacity>
            <Categories>Brinco</Categories>
          </TouchableOpacity>
        </CategoriesFlex>
        <Flex>
          <HomeProduct name={"Anel Ruby"} price={500} img={"anel2.jpg"} />
          <HomeProduct name={"Colar Diamante"} price={350} img={"Colar2.jpg"} />
        </Flex>
        <Flex>
          <HomeProduct name={"Brinco Ruby"} price={500} img={"Brinco.jpg"} />
          <HomeProduct name={"Brinco Ouro"} price={350} img={"brinco2.jpg"} />
        </Flex>
        <Blank></Blank>
      </Container>
      <BottomBar />
    </Screen>
  );
}
