import styled from "styled-components/native";

export const Title = styled.Text`
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  margin-top: 20px;
`;

export const Screen = styled.View`
  flex: 1;
  display: flex;
  align-items: center;
  background-color: white;
`;

export const Container = styled.View`
  width: 85%;
  margin-bottom: 100px;
`;

export const Grid = styled.View`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 30px;
`;

export const Flex = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 20px;
  gap: 40px;
`;

export const Text = styled.Text`
  font-size: 14px;
  margin-top: 30px;
`;

export const CategoriesFlex = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 30px;
  gap: 20px;
`;

export const Categories = styled.Text`
  font-size: 12px;
  border: 1px solid #a8a8a8;
  border-radius: 8px;
  padding: 10px;
  padding-left: 20px;
  padding-right: 20px;
`;

export const Blank = styled.View`
  width: 100px;
  height: 80px;
`;
