import React from "react";
import {
  Container,
  Header1,
  BotaoInteragir,
  CadastroTexto,
  Touch,
  Left,
} from "../../styles";
import TopTab from "../../components/Header";
import { useNavigation } from "@react-navigation/native";
import Endereço from "../../components/Endereço";
import SubHeader from "../../components/Subheader";

export default function PagEndereço() {
  const navigation = useNavigation();
  return (
    <Container>
      <Touch onPress={() => navigation.navigate("User")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>
      <SubHeader />
      <Header1>Endereços</Header1>

      <Endereço
        nome=""
        CEP=""
        Rua=""
        Número=""
        Bairro=""
        Complemento=""
      ></Endereço>

      <BotaoInteragir>
        <CadastroTexto>Adicionar endereço</CadastroTexto>
      </BotaoInteragir>
    </Container>
  );
}
