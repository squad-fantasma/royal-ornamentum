import React from "react";
import {
  Container,
  Header1,
  Header3,
  BotaoInteragir,
  CadastroTexto,
  Touch,
  Left,
  Text,
  Input,
  Container2,
  Screen,
} from "../../styles";
import Textoinput from "../../components/InputText";
import SubHeader from "../../components/Subheader";
import { useNavigation } from "@react-navigation/native";

export default function MudarSenha() {
  const navigation = useNavigation();
  return (
    <Screen>
      <SubHeader />
      <Touch onPress={() => navigation.navigate("User")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>
      <Container2>
        <Header1>Mudar senha</Header1>
        <Text>Senha atual</Text>
        <Input></Input>
        <Text>Nova senha</Text>
        <Input></Input>
        <Text>Confirmar nova senha</Text>
        <Input></Input>
        <BotaoInteragir>
          <CadastroTexto>Mudar senha</CadastroTexto>
        </BotaoInteragir>
      </Container2>
    </Screen>
  );
}
