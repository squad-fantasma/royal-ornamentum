import React from "react";
import {
  Container,
  Header1,
  BotaoInteragir,
  CadastroTexto,
  Touch,
  Left,
  Screen,
  Container2,
} from "../../styles";
import TopTab from "../../components/Header";
import Textoinput from "../../components/InputText";
import SubHeader from "../../components/Subheader";
import { useNavigation } from "@react-navigation/native";
import { Input, Text } from "./style";

export default function EsqueciMinhaSenha() {
  const navigation = useNavigation();
  return (
    <Screen>
      <Touch onPress={() => navigation.navigate("Login")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>
      <SubHeader />
      <Container2>
        <Text>Informe seu email</Text>
        <Input></Input>

        <BotaoInteragir
          onPress={() => navigation.navigate("EsqueciAlterarSenha")}
        >
          <CadastroTexto>Enviar email</CadastroTexto>
        </BotaoInteragir>
      </Container2>
    </Screen>
  );
}
