import React from "react";
import SubHeader from "../../components/Subheader";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  Flex,
  Left,
  Plus,
  Screen,
  Shadow,
  Text,
  TextDebito,
  TextProduct,
  Title,
  Touch,
} from "./style";

import PaymentProduct from "../../components/PaymentProduct/Index";

export default function PaymentMethod() {
  const navigation = useNavigation();
  return (
    <Screen>
      <Touch onPress={() => navigation.navigate("User")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>
      <SubHeader />
      <Container>
        <Title>Metodos de pagamento</Title>
        <Text>Cartão de credito</Text>
        <PaymentProduct />
        <TextDebito>Cartão de Debito</TextDebito>
        <PaymentProduct />
      </Container>
    </Screen>
  );
}
