import styled from "styled-components/native";

export const Screen = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  background-color: white;
`;

export const Container = styled.View`
  width: 85%;
`;

export const Title = styled.Text`
  font-size: 24px;
  font-weight: 600;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 30px;
`;

export const Touch = styled.TouchableOpacity`
  position: absolute;
  left: 10px;
  top: 38px;
  z-index: 1;
`;

export const Left = styled.Image`
  width: 50px;
  height: 50px;
`;

export const Shadow = styled.View`
  margin-top: 32px;
  padding: 10px;
  height: 114px;
  box-shadow: 0 2.4rem 4.8rem rgba(0, 0, 0, 0.075);
  display: flex;
  justify-content: center;
`;

export const Flex = styled.View`
  display: flex;
  flex-direction: row;
  align-items: start;
  justify-content: space-between;
`;

export const Text = styled.Text`
  font-size: 20px;
  font-weight: 600;
`;

export const TextDebito = styled.Text`
  font-size: 20px;
  font-weight: 600;
  margin-top: 40px;
`;

export const TextProduct = styled.Text`
  font-size: 20px;
  font-weight: 600;
`;

export const Plus = styled.Image`
  width: 40px;
  height: 40px;
`;
