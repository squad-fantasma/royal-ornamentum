import React from "react";
import {
  Container,
  Screen,
  Title,
  Shadow,
  Content,
  Button,
  ButtonText,
  Div,
} from "./style";

import { View, Image, TouchableOpacity } from "react-native";
import Header from "../../components/Header";

export default function Favorite() {
  return (
    <Screen>
      <Header />
      <Container>
        <Title>Favoritos</Title>
        <Shadow>
          <Content>
            Nenhum item adicionado ao favoritos, voltar a tela de home?
          </Content>
          <Div>
            <Button>
              <ButtonText>Voltar a home</ButtonText>
            </Button>
          </Div>
        </Shadow>
      </Container>
    </Screen>
  );
}
