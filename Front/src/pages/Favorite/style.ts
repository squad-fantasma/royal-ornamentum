import styled from "styled-components/native";

export const Screen = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  background-color: white;
`;

export const Container = styled.View`
  width: 85%;
`;

export const Title = styled.Text`
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  margin-top: 40px;
`;

export const Shadow = styled.View`
  margin-top: 38px;
  box-shadow: 0 2.4rem 4.8rem rgba(0, 0, 0, 0.075);
`;

export const Content = styled.Text`
  font-size: 20px;
  font-weight: 600;
  padding: 10px;
  padding-top: 35px;
  margin-bottom: 15px;
  text-align: center;
`;

export const Div = styled.View`
  display: flex;
  align-items: center;
  padding-bottom: 31px;
`;

export const Button = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  width: 70%;
  max-width: 400px;
  height: 50px;
  margin-top: 20px;
  border-radius: 16px;
  background-color: #8d3aec;
`;

export const ButtonText = styled.Text`
  font-size: 20px;
  line-height: 21;
  font-weight: 500;
  letter-spacing: 0.25;
  color: white;
  text-align: center;
`;
