import React from "react";
import {
  Container,
  Header1,
  BotaoInteragir,
  CadastroTexto,
  Touch,
  Left,
  Screen,
  Container2,
} from "../../styles";
import { Text, Input } from "./style";
import SubHeader from "../../components/Subheader";
import { useNavigation } from "@react-navigation/native";

export default function EsqueciAlterarSenha() {
  const navigation = useNavigation();
  return (
    <Screen>
      <Touch onPress={() => navigation.navigate("EsqueciMinhaSenha")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>
      <SubHeader />
      <Container2>
        <Header1>Alterar senha</Header1>

        <Text>Nova senha</Text>
        <Input></Input>

        <Text>Confirmar nova senha</Text>
        <Input></Input>

        <BotaoInteragir onPress={() => navigation.navigate("Login")}>
          <CadastroTexto>Alterar senha</CadastroTexto>
        </BotaoInteragir>
      </Container2>
    </Screen>
  );
}
