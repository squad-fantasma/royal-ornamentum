import styled from "styled-components/native";

export const Text = styled.Text`
  font-size: 16px;
  font-weight: 600;
  margin-top: 30px;
`;

export const Input = styled.TextInput`
  width: 100%;
  padding: 10px;
  background-color: #f1f2f5;
  border-radius: 8px;
  font-size: 14px;
  margin-bottom: 24px;
  margin-top: 20px;
`;
