import React from "react";
import SellProduct from "../../components/SellProduct";
import SubHeader from "../../components/Subheader";
import { Container, Left, Screen, Title, Touch } from "./style";
import { useNavigation } from "@react-navigation/native";

export function MySell() {
  const navigation = useNavigation();
  return (
    <Screen>
      <Touch onPress={() => navigation.navigate("User")}>
        <Left
          source={require("../../../assets/material-symbols_chevron-left-rounded.svg")}
        ></Left>
      </Touch>
      <SubHeader />
      <Container>
        <Title>Minhas vendas</Title>
        <SellProduct
          name={"Anel de ouro"}
          price={500}
          img={"sabrianna-NhrcL_C0sFA-unsplash 1"}
        />
        <SellProduct
          name={"Anel de ouro"}
          price={500}
          img={"sabrianna-NhrcL_C0sFA-unsplash 1"}
        />
      </Container>
    </Screen>
  );
}
