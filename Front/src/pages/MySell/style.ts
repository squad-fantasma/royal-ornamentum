import styled from "styled-components/native";

export const Screen = styled.View`
  flex: 1;
  display: flex;
  align-items: center;
  background-color: white;
`;

export const Container = styled.View`
  width: 85%;
  margin-bottom: 280px;
`;

export const Title = styled.Text`
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  margin-top: 40px;
`;

export const Touch = styled.TouchableOpacity`
  position: absolute;
  left: 10px;
  top: 38px;
  z-index: 1;
`;

export const Left = styled.Image`
  width: 50px;
  height: 50px;
`;
