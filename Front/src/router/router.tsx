import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Cadastro from "../pages/Cadastro";
import CompleteCart from "../pages/CompleteCart";
import CompleteFavorite from "../pages/CompleteFavorite";
import EsqueciAlterarSenha from "../pages/EsqueciAlterarSenha";
import EsqueciMinhaSenha from "../pages/EsqueciMinhaSenha";
import Home from "../pages/Home";
import InfoPesso from "../pages/InfoPesso";
import Login from "../pages/Login";
import MudarSenha from "../pages/MudarSenha";
import MudarEnde from "../pages/MudarEnde";
import { MySell } from "../pages/MySell";
import PagEndereço from "../pages/PagEndereço";
import PaymentMethod from "../pages/PaymentMethod";
import SellProduct from "../pages/SellProduct";
import User from "../pages/User";
import PagProduto from "../pages/PagProduto";

const Stack = createNativeStackNavigator();

function MyApp() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="CompleteFavorite"
          component={CompleteFavorite}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SellProduct"
          component={SellProduct}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="User"
          component={User}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="CompleteCart"
          component={CompleteCart}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MySell"
          component={MySell}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PaymentMethod"
          component={PaymentMethod}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Cadastro"
          component={Cadastro}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PagEndereço"
          component={PagEndereço}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PagProduto"
          component={PagProduto}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="EsqueciAlterarSenha"
          component={EsqueciAlterarSenha}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="EsqueciMinhaSenha"
          component={EsqueciMinhaSenha}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="InfoPesso"
          component={InfoPesso}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MudarSenha"
          component={MudarSenha}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MudarEnde"
          component={MudarEnde}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default MyApp;
