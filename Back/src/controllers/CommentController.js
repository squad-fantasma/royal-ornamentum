const { response } = require('express');
const Comment = require('../models/Comment');
const User = require('../models/User');
const Product = require('../models/Product');

//CRUDS

const create = async(req,res)=>{
    try{
        const comment = await Comment.create(req.body);
        return res.status(201).json({comment});
    }catch(err){
        return res.status(500).json({error:err});
    }
};

const index = async(req,res)=>{
    try{
        const comments = await Comment.findAll();
        return res.status(200).json({comments});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res)=>{
    const {id} = req.params;
    try{
        const comment = await Comment.findByPk(id);
        return res.status(200).json({comment});
    }catch(err){
        return response.status(500).json({err});
    }
 };

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Comment.update(req.body, {where: {id: id}});
        if(updated) {
            const comment = await Comment.findByPk(id);
            return res.status(200).send(comment);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Comentário não encontrado");
    }
};

 const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await Comment.destroy({where:{id:id}});
        if (deleted){
            return res.status(200).json("Comentário deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Comentário não encontrado.");
    }
 };

 //RELATIONSHIPS

 const addUser = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const comment = await Comment.findByPk(req.body.commentId);
        await comment.setUser(user);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeUser = async(req,res)=>{
    const {id} = req.params;
    try{
        const comment = await Comment.findByPk(id);
        await comment.setUser(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

const addProduct = async(req,res)=>{
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        const comment = await Comment.findByPk(req.body.commentId);
        await comment.setUser(product);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeProduct = async(req,res)=>{
    const {id} = req.params;
    try{
        const comment = await Comment.findByPk(id);
        await comment.setProduct(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

module.exports ={
    create,
    index,
    show,
    update,
    destroy,
    addUser,
    removeUser,
    addProduct,
    removeProduct
}