const { response } = require('express');
const Card = require('../models/Card');
const User = require('../models/User');

const create = async(req, res)=>{
    try{
        const card = await Card.create(req.body);
        return res.status(201).json({message:"Cartão cadastrado!",card:card});
    }catch(err){
        return res.status(500).json({error:err});
    }
 };

 const index = async(req,res)=>{
    try{
        const cards = await Card.findAll();
        return res.status(200).json({cards});
    }catch(err){
        return res.status(500).json({err});
    }
 };

 const show = async(req,res)=>{
    const {id} = req.params;
    try{
        const card = await Card.findByPk(id);
        return res.status(200).json({card});
    }catch(err){
        return response.status(500).json({err});
    }
 };

 const update = async(req,res)=>{
    const {id} = req.params;
    try{
        const [updated] = await Card.update(req.body,{where:{id:id}});
        if (updated){
            const card = await Card.findByPk(id);
            return res.status(200).send(card);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Cartão não encontrado.");
    }
 };

 const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await Card.destroy({where:{id:id}});
        if (deleted){
            return res.status(200).json("Cartão deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Cartão não encontrado.");
    }
 };

//RELATIONSHPIS

 const addUser = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const card = await Card.findByPk(req.body.cardId);
        await card.setUser(user);
        return res.status(200).json(card);
    }catch(err){
        return res.status(500).json({err});
    }
}

const removeUser = async(req,res)=>{
    const {id} = req.params;
    try{
        const card = await Card.findByPk(id);
        await card.setUser(null);
        return res.status(200).json(card);
    }catch(err){
        return res.status(500).json({err});
    }
}

 module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addUser,
    removeUser
 }