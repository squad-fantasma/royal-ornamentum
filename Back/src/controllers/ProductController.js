const { response } = require('express');
const Product = require('../models/Product');
const User = require('../models/User');


const create = async(req, res)=>{
    try{
        const product = await Product.create(req.body);
        return res.status(201).json({message:"Produto cadastrado!",product:product});
    }catch(err){
        return res.status(500).json({error:err});
    }
 };

 const index = async(req,res)=>{
    try{
        const products = await Product.findAll();
        return res.status(200).json({products});
    }catch(err){
        return res.status(500).json({err});
    }
 };

 const show = async(req,res)=>{
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    }catch(err){
        return response.status(500).json({err});
    }
 };

 const update = async(req,res)=>{
    const {id} = req.params;
    try{
        const [updated] = await Product.update(req.body,{where:{id:id}});
        if (updated){
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
 };

 const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await Product.destroy({where:{id:id}});
        if (deleted){
            return res.status(200).json("Produto deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
 };

 //RELATIONSHIPS

 const addUser = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.productId);
        await product.setUser(user);
        return res.status(200).json(product);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeUser = async(req,res)=>{
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        await product.setUser(null);
        return res.status(200).json(product);
    }catch(err){
        return res.status(500).json({err});
    }
};

//PHOTOS

const addPhoto = async(req,res)=>{
    const {id} = req.params;
    try{
        if(req.file){
            const path = process.env.APP_URL + "/uploads" + req.file.filename;
            console.log("path");
            
            const atributo = {
                photo: path
            }

            const [updated] = await Product.update(atributo,{where:{id:id}})
            if(updated)return res.status(200).json("foto adicionada com sucesso");
        }
    }catch(err){
        return res.status(500).json({err});
    }
};

const removePhoto = async(req,res)=>{
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        const pathDb = product.profilePic.split("/").slice(-1)[0];
        await fsPromise.unlink(path.join(__dirname,'..','..',"uploads",pathDb));
        const [updated] = await Product.update({photo:null },{where: {id:id}});
        if(updated)return res.status(200).json("foto removida.");
    }catch(err){
        return res.stauts(500).json({err});
    }
};



 module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addUser,
    removeUser,
    addPhoto,
    removePhoto
 }