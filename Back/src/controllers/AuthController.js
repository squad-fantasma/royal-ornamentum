const User = require("../models/User");
const Auth = require("../config/Auth");
const { validationResult } = require('express-validator');
const mailer = require('../config/transporter').mailer;
const path = require('path');
const hbs = require('handlebars');
const readHtml = require('../config/transporter').readHTMLFile;

const register = async(req,res)=>{
    try{
        const pathTemplate = path.resolve(__dirname,'..','..','templates');
        const generateHash = Auth.generatePassword(req.body.password);
        const salt = generateHash.salt;
        const hash = generateHash.hash;
        const newUserData = {
            name: req.body.name,
            birthDate: req.body.birthDate,
            email: req.body.email,
            cellphone: req.body.cellphone,
            type: req.body.type,
            hash: hash,
            salt: salt
        }
        const user = await User.create(newUserData);
        validationResult(req).throw();
        readHtml(path.join(pathTemplate, "index.html"), (err,html)=>{
			const template = hbs.compile(html);
			const replacements = {
				username: user.name
			};
			const htmlToSend = template(replacements);
			const message = {
				from: "trupefantasmasquad1@gmail.com",
				to: user.email,
				subject: "confirmação de cadastro",
				html: htmlToSend
			}
			mailer.sendMail(message, (err) => {
				console.log(err + "!");
			});
		});
        return res.status(201).json({message:"Usuário cadastrado!",user:user});
    }catch(err){
        return res.status(500).json({ error: err + '!'})
    }
};


const login = async(req,res)=>{
    try{
        const user = await User.findOne({where: {email: req.body.email}});
        if(!user)return res.status(404).json({message: "Usuário não encontrado"});
        const {password} = req.body;
        if(Auth.checkPassword(password, user.hash, user.salt)){
            const token = Auth.generateJWT(user);
            return res.status(200).json({acess_token: token});
        }else{
            return res.status(401).json({message: "senha inválida"});
        }
    }catch(error){
        return res.status(500).json({err: error});
    }
};

const getDetails = async(req,res)=>{
    try{
        const token = Auth.getToken(req);
        const payload = Auth.decodeJwt(token);
        const user = await User.findByPk(payload.sub);
        if(!user)return res.status(404).json({message: "Usuário não encontrado"});
        return res.status(200).json({user:user});
    }catch(e){
        return res.status(500).json({err: e});
    }
};

module.exports = {
    register,
    login,
    getDetails
};