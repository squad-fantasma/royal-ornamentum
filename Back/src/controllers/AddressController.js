const { response } = require('express');
const Address = require('../models/Address');
const User = require('../models/User');

const create = async(req, res)=>{
    try{
        const address = await Address.create(req.body);
        return res.status(201).json({message:"Endereço cadastrado!",address:address});
    }catch(err){
        return res.status(500).json({error:err});
    }
 };

 const index = async(req,res)=>{
    try{
        const addresses = await Address.findAll();
        return res.status(200).json({addresses});
    }catch(err){
        return res.status(500).json({err});
    }
 };

 const show = async(req,res)=>{
    const {id} = req.params;
    try{
        const address = await Address.findByPk(id);
        return res.status(200).json({address});
    }catch(err){
        return response.status(500).json({err});
    }
 };

 const update = async(req,res)=>{
    const {id} = req.params;
    try{
        const [updated] = await Address.update(req.body,{where:{id:id}});
        if (updated){
            const address = await Address.findByPk(id);
            return res.status(200).send(address);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Endereço não encontrado.");
    }
 };

 const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await Address.destroy({where:{id:id}});
        if (deleted){
            return res.status(200).json("Endereço deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Endereço não encontrado.");
    }
 };

 //RELATIONSHIPS

 const addUser = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const address = await Address.findByPk(req.body.addressId);
        await address.setUser(user);
        return res.status(200).json(address);
    }catch(err){
        return res.status(500).json({err});
    }
}

const removeUser = async(req,res)=>{
    const {id} = req.params;
    try{
        const address = await Address.findByPk(id);
        await address.setUser(null);
        return res.status(200).json(address);
    }catch(err){
        return res.status(500).json({err});
    }
}

 module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addUser,
    removeUser
 }