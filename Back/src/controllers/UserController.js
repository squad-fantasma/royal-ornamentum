 const { response } = require('express');
 const User = require('../models/User');
 const Address = require('../models/Address');
 const Product = require('../models/Product');
 const Card = require('../models/Card');
const { mailer } = require('../config/transporter');

//CRUDS

 const create = async(req, res)=>{
    try{
        const user = await User.create(req.body);
        const message ={
            to: user.email,
            subject: "Conta cadastrada!",
            text: "Seja bem vindo ao nosso marketplace, espero que se diverta comprando e vendendo no nosso aplicativo"
        }
        mailer.sendMail(message,(err)=>{
            console.log(err + "!");
        });
        return res.status(201).json({message:"Usuário cadastrado!",user:user});
    }catch(err){
        return res.status(500).json({error:err});
    }
 };

 const index = async(req,res)=>{
    try{
        const users = await User.findAll();
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
 };

 const show = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id,{include:[{model:Product},{model:Address},{model:Card}]});
        return res.status(200).json({user});
    }catch(err){
        return response.status(500).json({err});
    }
 };

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

 const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await User.destroy({where:{id:id}});
        if (deleted){
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }
 };

 //FAVORITES

 const favorite = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.productId);
        await user.addFavoriter(product);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
 };

 const unfavorite = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.productId);
        await user.removeFavoriter(product);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
 };

 const indexFavorites = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const allFavorites = await user.getFavoriter();
        return res.status(200).json({allFavorites});
    }catch(err){
        return res.status(500).json({err});
    }
 };

 //CART

 const cart = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.productId);
        await user.addBuyer(product);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
 };

 const removeCart = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.productId);
        await user.removeBuyer(product);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
 };

 const indexCart =  async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const allItens = await user.getBuyer();
        return res.status(200).json({allItens});
    }catch(err){
        return res.status(500).json({err});
    }
 };

//PHOTO

const addProfilePic = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        if(req.file){
            const path = process.env.APP_URL + "/uploads" + req.file.filename;
            console.log("path");
            
            const atributo = {
                profilePic: path
            }

            const [updated] = await user.update(atributo)
            if(updated)return res.status(200).json("foto adicionada com sucesso");
        }
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeProfilePic = async(req,res)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const pathDb = user.profilePic.split("/").slice(-1)[0];
        await fsPromise.unlink(path.join(__dirname,'..','..',"uploads",pathDb));
        const [updated] = await User.update({profilePic:null },{where: {id:id}});
        if(updated)return res.status(200).json("foto removida.");
    }catch(err){
        return res.stauts(500).json({err});
    }
};

 module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    favorite,
    unfavorite,
    indexFavorites,
    cart,
    removeCart,
    indexCart,
    addProfilePic,
    removeProfilePic
 }