const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Card = sequelize.define('Card', {
    name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    cardNumber:{
        type: DataTypes.STRING,
        allowNull: false
    },
    securityCode:{
        type: DataTypes.NUMBER,
        allowNull: false
    },
    validDate:{
        type: DataTypes.DATEONLY,
        allowNull: false
    }
});

Card.associate = function(models){
    Card.belongsTo(models.User);
}

module.exports = Card;