const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Address = sequelize.define('Address', {

    name:{
        type: DataTypes.STRING,
        allownull: false
    },
    street:{
        type: DataTypes.STRING,
        allownull: false
    },
    number:{
        type: DataTypes.STRING,
        allownull: false
    },
    CEP:{
        type: DataTypes.STRING,
        allownull: false
    },
    state:{
        type: DataTypes.STRING,
        allownull: false
    },
    city:{
        type: DataTypes.STRING,
        allownull: false
    },
    neighborhood:{
        type: DataTypes.STRING,
        allownull: false
    }
});

Address.associate = function(models){
    Address.belongsTo(models.User);
}

module.exports = Address;