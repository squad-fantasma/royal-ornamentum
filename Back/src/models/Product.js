//const faker = require("faker-br");
const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");
//const image = require('../../templates/anel2.jpg');
//import image as url from "../../templates/anel2";

const Product = sequelize.define('Product', {

    name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    price:{
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    amount:{
        type: DataTypes.INTEGER,
        allowNull: false
    },
    category:{
        type: DataTypes.STRING,
        allowNull: false
    },
    description:{
        type: DataTypes.TEXT,
        allowNull: true
    },
    photo:{
        type: DataTypes.TEXT,
        allowNull: true,
    }
});

Product.associate = function(models){
    Product.belongsTo(models.User);

    Product.belongsToMany(models.User,{
        through: 'favorite',
        as: 'favorited',
        foreingKey: 'favoritedId'
    });
    Product.belongsToMany(models.User,{
        through: 'cart ',
        as: 'sell',
        foreingKey: 'sellId'
    });
    Product.belongsToMany(models.User,{
        through: models.Comment,
        as: 'commented'
    });
};

module.exports = Product;