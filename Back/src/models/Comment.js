const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Comment = sequelize.define('Comment',{

    id:{
       type: DataTypes.INTEGER,
       autoIncrement: true,
       primaryKey: true 
    },
    comment:{
        type: DataTypes.TEXT,
        allowNull: false
    }
});



module.exports = Comment;