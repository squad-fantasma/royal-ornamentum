const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {

    name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    birthDate:{
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    email:{
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    cellphone:{
        type: DataTypes.NUMBER,
        allowNull: false,
        unique: true
    },
    hash:{
        type: DataTypes.STRING,
        allowNull: false
    },
    salt:{
        type: DataTypes.STRING,
        allowNull:false
    },
    type:{
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 0
    },
    profilePic:{
        type: DataTypes.TEXT,
        allowNull:true
    }
});

User.associate = function(models){
    User.hasMany(models.Product);
    User.hasMany(models.Address);
    User.hasMany(models.Card);

    User.belongsToMany(models.Product,{
        through:models.Comment,
        as: 'commentarist'
    });
    
    User.belongsToMany(models.Product,{
        through: 'favorite',
        as: 'favoriter',
        foreingKey:'favoriterId'
    });

    User.belongsToMany(models.Product,{
        through: 'cart',
        as: 'buyer',
        foreingKey: 'buyerId'
    });
};

module.exports = User;