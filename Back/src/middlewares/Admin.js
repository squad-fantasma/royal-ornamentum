const User = require('../models/User');

const admin = async(res,req,next)=>{
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        if (user.type == 1) return next();
        else return res.status(401).json({'error': 'Sem autorização.'})
    }catch(err){
        return res.status(500).json({err});
    }
};


module.exports = admin;