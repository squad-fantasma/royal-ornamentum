const { body, validationResult } = require('express-validator');


const validationUser = (method) =>{
    switch(method){
        case 'create':{
            return[
                body('name').isLength({min: 1}).withMessage("esse campo deve ser preenchido."),
                body('birthDate').isDate({format: 'DD/MM/YYYY'}).withMessage("dia/mês/ano"),
                body('email').isEmail().withMessage("esse campo deve ser preenchido"),
                body('cellphone').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('password').isLength({min: 7}).withMessage("sua senha deve conter pelo menos 8 (oito) caracteres")
            ];
        };
    
        case 'update':{
            return[
                body('name').isLength({min: 1}).withMessage("esse campo deve ser preenchido."),
                body('birthDate').isDate({format: 'DD/MM/YYYY'}).withMessage("dia/mês/ano"),
                body('email').isEmail().withMessage("esse campo deve ser preenchido"),
                body('cellphone').isLength({min: 1}),
                body('password').isLength({min: 8}).withMessage("sua senha deve conter pelo menos 8 (oito) caracteres")
            ];
        }
    };
};

const validationProduct = (method) =>{
    switch(method){
        case 'create':{
            return [
                body('name').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('price').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('amount').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('category').isLength({min: 1}).withMessage("esse campo deve ser preenchido")
            ];
        };
        case 'update':{
            return [
                body('name').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('price').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('amount').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('category').isLength({min: 1}).withMessage("esse campo deve ser preenchido")
            ];
        };
    };
};

const validationAddress = (method) =>{
    switch(method){
        case 'create':{
            return [
                body('name').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('street').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('CEP').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('state').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('city').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('neighborhood').isLength({min: 1}).withMessage("esse campo deve ser preenchido")
            ];
        };
        case 'update':{
            return [
                body('name').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('street').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('CEP').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('state').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('city').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('neighborhood').isLength({min: 1}).withMessage("esse campo deve ser preenchido")
            ];
        };
    };
};

const validationCard = (method) =>{
    switch(method){
        case 'create':{
            return[
                body('name').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('cardNumber').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('securityCode').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('validDate').isLength({min: 1}).withMessage("esse campo deve ser preenchido")
            ];
        };
        case 'update':{
            return[
                body('name').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('cardNumber').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('securityCode').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
                body('validDate').isLength({min: 1}).withMessage("esse campo deve ser preenchido")
            ];
        };
    };
};

const validationComment = (method) =>{
    switch(method){
        case 'create':{
            return[
                body('comment').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
            ];
        };
        case 'update':{
            return[
                body('comment').isLength({min: 1}).withMessage("esse campo deve ser preenchido"),
            ];
        };
    };
};

const errorTreatment = (req,res,next) =>{
    const erros = validationResult(req);
    if(!erros.isEmpty()){
        return res.status(422).json({erros: erros.array()});
    }
    next();
};

module.exports ={
    validationUser,
    validationAddress,
    validationProduct,
    validationCard,
    validationComment,
    errorTreatment
}; 