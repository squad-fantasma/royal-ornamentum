const nodemailer = require("nodemailer");
const fs = require("fs");
require("./dotenv")();
const mailer = nodemailer.createTransport({
    service: 'gmail',
    auth:{
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD,
    }
});

const readHTMLFile = function(path,callback){
    fs.readFile(path,{encoding: 'utf-8'},function(err,html){
        if(err){
            throw err;
            callback(err);
        }else{
            callback(null,html);
        }
    });
};

module.exports = {
    mailer,
    readHTMLFile
};