const Product = require('../../models/Product');
const faker = require('faker-br');

const seedProduct = async function(){
    try{
        await Product.sync({force: true});

        for(let i = 0,j = 0;i <= 20;i++){
            if(i%6==0){j++};
            await Product.create({
                name: faker.name.firstName(),
                price: faker.commerce.price(),
                amount: faker.random.number(),
                category: faker.name.lastName(),
                description: faker.name.jobDescriptor(),
                UserId: j
            });
        }
    }catch(err){
        return console.log(err);
    }
}

module.exports = seedProduct;