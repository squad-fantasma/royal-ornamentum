//const UserController = require('../../controllers/AuthController');
const Auth = require('../../config/Auth');
const User = require('../../models/User');
const faker = require('faker-br');

const seedUser = async function(){
    try{
        await User.sync({force: true});
        
        const generateHash = Auth.generatePassword(faker.internet.password());
        for(let i = 0;i <= 20;i++){
            
            await User.create({
                name: faker.name.firstName(),
                birthDate: faker.date.past(),
                email: faker.internet.email(),
                cellphone: faker.phone.phoneNumber(),
                hash: generateHash.hash,
                salt: generateHash.salt,
                profilePic: faker.image.avatar()
            });
        }
        for(let i = 0;i <= 5;i++){
            await User.create({
                name: faker.name.firstName(),
                birthDate: faker.date.past(),
                email: faker.internet.email(),
                cellphone: faker.phone.phoneNumber(),
                hash: generateHash.hash,
                salt: generateHash.salt,
                type: true,
                profilePic: faker.image.avatar()
            });
        }
    }catch(err){
        return console.log(err);
    }
}

module.exports = seedUser;