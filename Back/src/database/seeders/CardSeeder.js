const Card = require('../../models/Card');
const faker = require('faker-br');

const seedCard = async function(){
    try{
        await Card.sync({force: true});

        for(let i = 0;i <= 20;i++){
            await Card.create({
                name: faker.name.firstName(),
                cardNumber: faker.random.number(),
                securityCode: faker.random.number(),
                validDate: faker.date.past(),
                UserId: i+1
            });
        }
    }catch(err){
        return console.log(err);
    }
}

module.exports = seedCard;