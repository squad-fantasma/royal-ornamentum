const Address = require('../../models/Address');
const faker = require('faker-br');

const seedAddress = async function(){
    try{
        await Address.sync({force: true});

        for(let i = 0;i <= 20;i++){
            await Address.create({
                name: faker.name.firstName(),
                street: faker.address.streetName(),
                number: faker.random.number(),
                CEP: faker.random.number(),
                state: faker.address.state(),
                city: faker.address.city(),
                neighborhood: faker.address.streetName(),
                UserId: i+1
            });
        }
    }catch(err){
        return console.log(err);
    }
}

module.exports = seedAddress;