const Comment = require('../../models/Comment');
const faker = require('faker-br');

const seedComment = async function(){
    try{
        await Comment.sync({force: true});

        for(let i = 0;i <= 20;i++){
            for(let j = 0;j <= 2;j++){
                await Comment.create({
                    comment: faker.lorem.text(),
                    UserId: j+1,
                    ProductId: i+1
                });
            }
        }
    }catch(err){
        return console.log(err);
    }
}

module.exports = seedComment;