require('../../config/dotenv')();
require('../../config/sequelize');

const seedUser = require('./UserSeeder');
const seedProduct = require('./ProductSeeder');
const seedAddress = require('./AddressSeeder');
const seedCard = require('./CardSeeder');
const seedComment = require('./CommentSeeder');

(async () => {
  try {
    await seedUser();
    await seedProduct();
    await seedAddress();
    await seedCard();
    await seedComment();

  } catch(err) { console.log(err) }
})();
