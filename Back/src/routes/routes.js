const { Router } = require('express');
const UserController = require('../controllers/UserController');
const AuthController = require('../controllers/AuthController');
const ProductController = require('../controllers/ProductController');
const AddressController = require('../controllers/AddressController');
const CardController = require('../controllers/CardController');
const CommentController = require('../controllers/CommentController')
const Validator = require('../config/validator');
const multer = require('multer');
const storage = require('../config/files');
const upload = multer({storage: storage,fileFilter: function(req,file,cb){
    const ext = path.extname(file.originalname);
    if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg'){
        return cb(new Error('Algo estava errado'), false)
    }
    cb(null,true);
},limits:{
    fileSize: 2048 * 2048
}
});
const allUploads = upload.fields([{name: 'photo', maxCount: 6}]);
const router = Router();

//ROTAS

//USER
router.get('/users',UserController.index);
router.get('/user/:id',UserController.show);
//router.post('/user',UserController.create);
router.post('/user/register',Validator.validationUser('create'),Validator.errorTreatment,AuthController.register);
router.put('/profilePic/user/:id',upload.single('photo'),UserController.addProfilePic);
router.put('/user/:id',Validator.validationUser('update'),Validator.errorTreatment,UserController.update);
router.delete('/user/:id',UserController.destroy);

router.post('/user/login',AuthController.login);

//PRODUCT
router.get('/products',ProductController.index);
router.get('/product/:id',ProductController.show);
router.post('/product',Validator.validationProduct('create'),Validator.errorTreatment,ProductController.create);
router.post('/photos/product/:id',allUploads,ProductController.addPhoto);
router.put('/product/:id',Validator.validationProduct('update'),Validator.errorTreatment,ProductController.update);
router.delete('/product/:id',ProductController.destroy);

//ADDRESS
router.get('/addresses',AddressController.index);
router.get('/address/:id',AddressController.show);
router.post('/address',Validator.validationAddress('create'),Validator.errorTreatment,AddressController.create);
router.put('/address/:id',Validator.validationAddress('update'),Validator.errorTreatment,AddressController.update);
router.delete('/address/:id',AddressController.destroy);

//CARD
router.get('/cards',CardController.index);
router.get('/card/:id',CardController.show);
router.post('/card',Validator.validationCard('create'),Validator.errorTreatment,CardController.create);
router.put('/card/:id',Validator.validationCard('update'),Validator.errorTreatment,CardController.update);
router.delete('/card/:id',CardController.destroy);

//COMMENTS
router.get('/comments',CommentController.index);
router.get('/comment/:id',CommentController.show);
router.post('/comment',Validator.validationComment('create'),Validator.errorTreatment,CommentController.create);
router.put('/comment/:id',Validator.validationComment('update'),Validator.errorTreatment,CommentController.update);
router.delete('/comment/:id',CommentController.destroy);

//RELATIONSHIPS

//USER-PRODUCT
router.put('/product/addUser/user/:id',ProductController.addUser);
router.put('/product/removeUser/product/:id',ProductController.removeUser);

//USER-ADDRESS
router.put('/address/addAddress/user/:id',AddressController.addUser);
router.put('/address/removeAddress/address/:id',AddressController.removeUser);

//USER-CARD
router.put('/card/addUser/user/:id',CardController.addUser);
router.put('/card/removeUser/card/:id',CardController.removeUser);

//FAVORITES

router.put('/user/favorite/user/:id',UserController.favorite);
router.put('/user/unfavorite/user/:id',UserController.unfavorite);
router.get('/user/indexFavorites/user/:id',UserController.indexFavorites);

//CART

router.put('/user/cart/user/:id',UserController.cart);
router.put('/user/removeCart/user/:id',UserController.removeCart);
router.get('/user/indexCart/user/:id',UserController.indexCart);


module.exports = router;