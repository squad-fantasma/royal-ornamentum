# Royal Ornamentum

Projeto de Marktplace em forma de aplicativo
focado na compra e venda de jois e artigos de luxo .

**Status do Projeto** : Terminado

![Badge](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![Badge](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)

## Tabela de Conteúdo

*Faça um índice com links internos para todos os tópicos seguintes.*

1. [Tecnologias utilizadas](#tecnologias-utilizadas)

2. [Download](#Download)

3. [Instalação](#Instalação)

4. [Configurações](#Configurações)

5. [Execução](#Execução)

6. [Arquitetura](#arquitetura)

7. [Autores](#autores)

<br>
<hr>

## Tecnologias utilizadas

Essas são as frameworks e ferramentas que você precisará instalar para desenvolver esse projeto:

**[Node.js](https://nodejs.org/en/)**  
**[React](https://pt-br.reactjs.org/)**  
**[Expo](https://expo.dev/)**   
**[Sequelize](https://sequelize.org/)**

<br>
<hr>

##  Download

*Para que seja possível a execução dos arquivos deste repositório, o usuário deve clonar através da ferramenta **[git](https://git-scm.com/downloads)**. Abrindo o terminal do seu sistema operacional ou o GitBash, insira o seguinte comando na pasta desejada:*

``` git
git clone https://gitlab.com/squad-fantasma/royal-ornamentum.git
```

## Instalação

Para o correto funcionamento do aplicativo, terão que ser feitas as instalações das dependências, tanto da pasta `back-end`, quanto da pasta `front-end`. Para isso entre na pasta que foi clonada pelo comando e exclua a pasta `.git`:

``` bash
cd royal-ornamentum
rm -r .git
```

### Na pasta `back-end`

Abra o seu terminal e execute o comando para instalar as dependências da pasta `back-end`.

``` bash
cd Back
npm install
```

### Na pasta `front-end`

Agora, a partir do passo anterior, execute os comandos abaixo para instalar as dependências da pasta `front-end`.

``` bash
cd ..
cd Front
yarn install 
```

<br>
<hr>

## Configurações

Após a instalação, algumas preparações anteriores devem ser realizadas na pasta `back-end`.

A partir dos comandos abaixo, será feita a configuração da pasta `back-end`:

```bash
cd ..
cd back-end
cp .env_example .env
npm run keys
npm run migrate
npm run seed
```

##  Execução

Ainda na pasta `back-end`, execute o seguinte comando para servir o aplicativo em um servidor customizado para posterior execução no front-end:

``` bash
npm run dev ou npm start

Com as configurações feitas, mude a seguir para a pasta `front-end`, para a execução do aplicativo utilizando o **Expo** utilizando os seguintes comandos:

``` bash
cd ..
cd Front
expo start

```
Para parar a execução do aplicativo, basta executar o comando `CTRL + C` no terminal.

<br>
<hr>

- [Pesquisa Desk / User Flow](https://www.figma.com/file/3FIfx345YAeNlmuhIGHL0A/Trabalho-Final-Squad-1?node-id=0-1&t=iI8J8LFM7VRAyEiS-0)

- [Trello](https://trello.com/b/IDwJsXVF/trupe-fantasma)

- [Figma](https://www.figma.com/file/IyhohgHlN8u90mRcoOu3zk/Processo-de-UX---Trupe-Fantasma?node-id=0%3A1&t=oXsJ59iqkehxRBRr-1)

- [Modelagem]-(https://app.brmodeloweb.com/#!/conceptual/64146efaa25221fec5e22775)

## Autores


* Gerente - Bruna Andrade

* Gerente -João Rijo

* Tech Lead - Rian manhente

* Tech Lead - Breno Natal

* Tech Lead - Letícia Dayane

* Dev Front-end - Antonio Nazar

* Dev Front-end - Eduardo Bensabat

* Dev Back-end - Gabriel 

